using System;
using MarziBits.Deconstructs;
using System.Drawing;
using Xunit;
namespace MarziBits.Tests
{
    public class DeconstructsTests
    {
        [Fact]
        public void DateTime()
        {
            var dt = new DateTime(2019, 12, 13);
            var (d, m, y) = dt;

            Assert.Equal(dt.Day, d);
            Assert.Equal(dt.Month, m);
            Assert.Equal(dt.Year, y);
        }

        [Fact]
        public void ColorToRGBA()
        {
            var cyan = Color.Cyan;
            var (r, g, b, a) = cyan;

            Assert.Equal(cyan.R, r);
            Assert.Equal(cyan.G, g);
            Assert.Equal(cyan.B, b);
            Assert.Equal(cyan.A, a);
        }
    }
}