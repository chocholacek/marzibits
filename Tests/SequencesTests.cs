using System.Linq;
using MarziBits;
using MarziBits.Extenstions;
using Xunit;
using Shouldly;

namespace MarziBits.Tests
{
    public class SequencesTests
    {
        [Fact]
        public void FibonacciSequence1Number()
        {
            var actual = Sequences.FibonacciSequence.First();
            Assert.Equal(0, actual);
        }

        [Fact]
        public void FibonacciSequence2Numbers()
        {
            var expected = new [] { 0, 1 };
            var actual = Sequences.FibonacciSequence.Take(2).ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void FibonacciSequence10Numbers()
        {
            var expected = new [] { 0, 1, 1, 2, 3, 5, 8, 13, 21, 34 };
            var actual = Sequences.FibonacciSequence.Take(10).ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AlternatingSequence2Elements()
        {
            var expected = new [] { 0, 1 };
            var actual = Sequences.AlternatingSequence(0, 1).Take(2).ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void AlternatingSequence4Elements()
        {
            var expected = new[] { 0, 1, 0, 1 };
            var actual = Sequences.AlternatingSequence(0, 1).Take(4).ToList();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RepeatedCharacters1Char()
        {
            var expected = new string('a', 5);
            var actual = Sequences.RepeatedCharacters("a").Take(5).Join();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RepeatedCharactersMultipleChars()
        {
            var expected = "abcdeabcdeabcdeabc";
            var actual = Sequences.RepeatedCharacters("abcde").Take(18).Join();

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SquareNumbers()
        {
            var expected = new [] { 1, 4, 9, 16, 25, 36, 49 };
            var actual = Sequences.SquareNumbers.Take(expected.Length);

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArithmeticSequence0To10Step1()
        {
        //Given
            var expected = Enumerable.Range(0, 10);
            var actual = Sequences.ArithmeticSequence().Take(10);
        //When
        
        //Then
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void ArithmeticSequence0To10Step2()
        {
            //Given
            var expected = Enumerable.Range(0, 10).Where(x => x % 2 == 0);
            var actual = Sequences.ArithmeticSequence(0, 2).Take(expected.Count());
        //When

        //Then
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EvenNumbers()
        {
            var expected = Enumerable.Range(0, 100).Where(x => x % 2 == 0);
            var actual = Sequences.EvenNumbers.Take(expected.Count());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void OddNumbers()
        {
            var expected = Enumerable.Range(0, 100).Where(x => x % 2 != 0);
            var actual = Sequences.OddNumbers.Take(expected.Count());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GeometricSequenceSameQ()
        {
            var expected = Enumerable.Repeat(1, 10);
            var actual = Sequences.GeometricSequence(1, 1).Take(expected.Count());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GeometricSequenceQ3()
        {
            var expected = new [] { 1, 3, 9, 27 };
            var actual = Sequences.GeometricSequence(1, 3).Take(expected.Count());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PowerSequenceSquares()
        {
            var expected = new[] { 1, 2, 4, 8 };
            var actual = Sequences.PowerSequence(2).Take(expected.Count());

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PowerSequenceCubes()
        {
            var expected = new[] { 1, 3, 9, 27 };
            var actual = Sequences.PowerSequence(3).Take(expected.Count());

            Assert.Equal(expected, actual);
        }
    }
}
