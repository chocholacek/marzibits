using System;

namespace MarziBits.AsyncLogger
{
    public interface IAsyncLogger : IDisposable
    {
         void Log(object obj);
    }
}