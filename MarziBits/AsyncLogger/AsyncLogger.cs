using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MarziBits.AsyncLogger
{
    public class AsyncLogger : IAsyncLogger
    {
        private Task task;

        private CancellationTokenSource tokenSource = new CancellationTokenSource();

        private CancellationToken token => tokenSource.Token;

        private Queue<object> objects = new Queue<object>();

        public AsyncLogger()
        {
            task = Task.Run(process);
        }

        public void Dispose()
        {
            tokenSource.Cancel();
            foreach (var obj in objects)
            {
                Console.WriteLine(obj);
            }
        }

        public void Log(object obj)
        {
            if (token.IsCancellationRequested)
                return;
            objects.Enqueue(obj);
        }

        private void process()
        {
            Task.Delay(5000);
            while (!token.IsCancellationRequested)
            {
                if (objects.Any())
                    Console.WriteLine(objects.Dequeue());
            }
        }
    }
}