using System;
using System.Collections.Generic;
using System.Linq;

namespace MarziBits
{
    public static class Sequences
    {
        /// <summary>
        /// General generator used to generate pseudo-infinite sequences
        /// </summary>
        /// <param name="seed">initial state</param>
        /// <param name="generator">generator</param>
        /// <param name="resultSelector">result selector</param>
        /// <typeparam name="TState">state type</typeparam>
        /// <typeparam name="TResult">result type</typeparam>
        /// <returns></returns>
        public static IEnumerable<TResult> SequenceGenerator<TState, TResult>(TState seed, Func<TState, TState> generator, Func<TState, TResult> resultSelector)
        {
            while (true)
            {
                yield return resultSelector(seed);
                seed = generator(seed);
            }
        }

        /// <summary>
        /// Implementation of Fibonacci sequence
        /// 
        /// usage: Sequences.FibonacciSequence.Take(10)
        /// </summary>
        /// <returns>[0, 1, 1, 2, 3, 5, 8, 13, ...]</returns>
        public static IEnumerable<int> FibonacciSequence => SequenceGenerator(
            (curr: 0, next: 1), 
            tuple => (curr: tuple.next, next: tuple.curr + tuple.next), 
            tuple => tuple.curr);

        public static IEnumerable<TItem> AlternatingSequence<TItem>(TItem first, TItem second) => SequenceGenerator(
            (curr: first, next: second),
            tuple => (curr: tuple.next, next: tuple.curr),
            tuple => tuple.curr);

        /// <summary>
        /// Repeats characters
        /// 
        /// example: RepeatedCharacters("hey").Take(5) => "heyhe"
        /// </summary>
        /// <param name="seed">string containing repeating characters</param>
        /// <returns></returns>
        public static IEnumerable<char> RepeatedCharacters(string seed) => SequenceGenerator(
            (i: 0, seed: seed),
            tuple => (i: (tuple.i + 1) % tuple.seed.Length, seed: tuple.seed),
            tuple => tuple.seed[tuple.i]);

        public static IEnumerable<char> RepeatedCharacters(IEnumerable<char> characters) 
            => RepeatedCharacters(new string(characters.ToArray()));

        /// <summary>
        /// Implementation of square numbers
        /// </summary>
        /// <returns>[1, 4, 8, 16, 25, 36, 49, ...]</returns>
        public static IEnumerable<int> SquareNumbers => SequenceGenerator(
            1,
            i => i + 1,
            i => i * i);

        
        /// <summary>
        /// Implementaion of arithmetic sequence
        /// </summary>
        /// <param name="start">start of the sequence</param>
        /// <param name="d">common difference</param>
        /// <returns></returns>
        public static IEnumerable<int> ArithmeticSequence(int start = 0, int d = 1) => SequenceGenerator(
            (curr: start, d: d),
            tuple => (curr: tuple.curr + tuple.d, d: tuple.d),
            tuple => tuple.curr);

        public static IEnumerable<int> EvenNumbers => ArithmeticSequence(0, 2);

        public static IEnumerable<int> OddNumbers => ArithmeticSequence(1, 2);

        /// <summary>
        /// Implementation of geometric sequence
        /// </summary>
        /// <param name="start">starting value</param>
        /// <param name="q">quocient</param>
        /// <returns></returns>
        public static IEnumerable<int> GeometricSequence(int start = 1, int q = 2) => SequenceGenerator(
           (curr: start, q: q),
           tuple => (curr: tuple.curr * tuple.q, q: tuple.q),
           tuple => tuple.curr);

        /// <summary>
        /// Generated sequence in form of n^x, where n is the function parameter and x is incremented from 0
        /// 
        /// Example:
        ///     PowerSequence(2) => 2^x => [1, 2, 4, 8, 16, 32, ...]
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static IEnumerable<int> PowerSequence(int n) => SequenceGenerator(
           (n: n, i: 0),
           tuple => (n: tuple.n, i: tuple.i + 1),
           tuple => (int)Math.Pow(tuple.n, tuple.i));
    }
}