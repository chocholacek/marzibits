using System.Drawing;

namespace MarziBits.Deconstructs
{
    public static class ColorDeconstructs
    {
        /// <summary>
        /// Deconstructs color into simple RGBA
        /// </summary>
        /// <param name="color"></param>
        /// <param name="r">red</param>
        /// <param name="g">green</param>
        /// <param name="b">blue</param>
        /// <param name="a">alpha</param>
        public static void Deconstruct(this Color color, out byte r, out byte g, out byte b, out byte a) 
            => (r, g, b, a) = (color.R, color.G, color.B, color.A); 
    }
}