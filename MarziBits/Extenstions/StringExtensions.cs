using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarziBits.Extenstions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Converts hex string into byte array.
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static byte[] ToBytes(this string hex)
        {
            if (hex.Length % 2 != 0)
                throw new ArgumentException("arument doesn't have even number of characters");

            IEnumerable<int> alternateRange(int from, int to)
            {
                for (; from < to; from += 2)
                    yield return from;
            }

            int nc = hex.Length;
            byte[] bytes = new byte[nc / 2];
            foreach (var i in alternateRange(0, nc))
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        /// <summary>
        /// Join collection of characters into string.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static string Join(this IEnumerable<char> collection)
            => new string(collection.ToArray());

        /// <summary>
        /// Joins collection of strings into one string.
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        public static string Join(this IEnumerable<string> collection)
            => collection.Aggregate(new StringBuilder(), (sb, s) => sb.Append(s))
                .ToString();

        /// <summary>
        /// Turns string into hex string, treating 0-9 as integers not ASCII values of characters
        /// Example:
        ///     1234 => 01020304
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string ToHexaString(this string str) => str.Select(c => $"0{c}").Join();
    }
}