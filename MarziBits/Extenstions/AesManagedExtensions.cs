using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MarziBits.Extenstions
{
    public static class AesManagedExtensions
    {
        /// <summary>
        /// Bitwise XOR of two byte arrays.
        /// </summary>
        /// <param name="arr1"></param>
        /// <param name="arr2"></param>
        /// <returns>result of bitwise XOR on two arrays</returns>
        public static byte[] XOR(this byte[] arr1, byte[] arr2)
        {
            if (arr1.Length != arr2.Length)
                throw new ArgumentException("various array lengths");

            byte xor(byte x, byte y) => (byte)(x ^ y);

            return arr1.Zip(arr2, xor).ToArray();
        }

        /// <summary>
        /// Encrypts data using class-specified Key and Seed.
        /// </summary>
        /// <param name="data"></param>
        /// <returns>aes encrypted 16B of data</returns>
        public static async Task<byte[]> Encrypt(this AesManaged aes, byte[] data)
        {
            var enc = aes.CreateEncryptor(); // creates aes encryptor with stored key and v

            using (var ms = new MemoryStream())
            using (var cs = new CryptoStream(ms, enc, CryptoStreamMode.Write))
            {
                await cs.WriteAsync(data, 0, data.Length);
                cs.FlushFinalBlock();

                return ms.ToArray();
            }
        }
    }
}