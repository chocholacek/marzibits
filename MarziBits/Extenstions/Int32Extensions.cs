namespace MarziBits.Extenstions
{
    public static class Int32Extensions
    {
        /// <summary>
        /// Creates hex string from inter with size of 16B padded from left with zeroes.
        /// Exmaple: 
        ///     1 => "00...01"
        ///     2 => "00...02"
        ///    10 => "00...0a"
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string ToHexaString(this int c) => $"{c:x}".PadLeft(32, '0');
    }
}