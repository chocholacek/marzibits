using System.Collections.Generic;
using System.Linq;

namespace MarziBits.Extenstions
{
    public static class IEnumerableExtensions
    {
        public static (T min, T max) MinMax<T>(this IEnumerable<T> enumerable) => (enumerable.Min(), enumerable.Max());
    }
}